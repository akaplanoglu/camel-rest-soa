# Camel Rest-Soa App
This application has one camel-rest endpoint and cxf-jaxrs.
Application routes the request to the cxf endpoint doing some processes like transforming, converting data. 

## Prerequisites
1. Java 1.8
2. Maven 3.5.1

### Command to start the project

`spring-boot:run` --or--  run directly from CamelRestSoaApplication.java

## Notes

1. I have implemented 2 routes because I was not sure about the mechanism of the task.
2. The first one which is currently running is getting request from rest endpoint and transfer data to cxf endpoint.
3. I have reversed "message" string and get back to the client as response.
4. The second one is currently commented in "EngineRoute.java". 
5. If you comment the current one and open the other one you'll see it gets request from client and returns a response immediately.
6. In the meantime the route calls cxf endpoint and get response from it.
7. I attached postman req. You can call get-engine request.