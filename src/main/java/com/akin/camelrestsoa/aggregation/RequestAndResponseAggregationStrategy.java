package com.akin.camelrestsoa.aggregation;

import com.akin.camelrestsoa.route.request.EngineDto;
import com.akin.camelrestsoa.rs.model.ResponseWrapper;
import org.apache.camel.AggregationStrategy;
import org.apache.camel.Exchange;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;

public class RequestAndResponseAggregationStrategy implements AggregationStrategy {
    public Exchange aggregate(Exchange orig, Exchange httpExchange) {
        EngineDto originalBody = (EngineDto) orig.getIn().getBody();
        InputStream httpResp = (InputStream) httpExchange.getIn().getBody();

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ResponseWrapper.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            ResponseWrapper res = (ResponseWrapper) jaxbUnmarshaller.unmarshal(httpResp);
            originalBody.setMessage(res.getStatus());
        } catch (JAXBException e) {
            // TODO log instead of printing stacktrace
            e.printStackTrace();
        }

        httpExchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/json");
        httpExchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, 200);
        httpExchange.getMessage().setBody(originalBody);
        return httpExchange;
    }
}
