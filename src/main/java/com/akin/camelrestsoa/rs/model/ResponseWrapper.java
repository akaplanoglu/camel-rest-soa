package com.akin.camelrestsoa.rs.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ResponseWrapper")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseWrapper {

    private static final long serialVersionUID = -3501772243949297054L;

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
