package com.akin.camelrestsoa.rs.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "CxfApiRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class CxfApiRequest implements Serializable {
    private static final long serialVersionUID = -3501772243949297054L;

    @JacksonXmlProperty(isAttribute = true)
    private int id;
    @JacksonXmlProperty(isAttribute = true)
    private String message;

    public CxfApiRequest(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public CxfApiRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "CxfApiRequest{" +
                "id=" + id +
                ", message='" + message + '\'' +
                '}';
    }
}
