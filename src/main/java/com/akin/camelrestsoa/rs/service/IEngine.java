package com.akin.camelrestsoa.rs.service;

import com.akin.camelrestsoa.rs.model.CxfApiRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("")
@Produces(MediaType.WILDCARD)
@Consumes(MediaType.WILDCARD)
public interface IEngine {

    @Path("/{id}/{message}")
    @GET
    Response get(@PathParam("id") int id, @PathParam("message") String message);

    @GET
    @Path("")
    Response get(CxfApiRequest cxfApiRequest);

    /*@Path("/test")
    @GET
    String get(String input);*/

}
