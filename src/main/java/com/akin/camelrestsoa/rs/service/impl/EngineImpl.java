package com.akin.camelrestsoa.rs.service.impl;

import com.akin.camelrestsoa.rs.model.CxfApiRequest;
import com.akin.camelrestsoa.rs.model.ResponseWrapper;
import com.akin.camelrestsoa.rs.service.IEngine;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;

@Service
public class EngineImpl implements IEngine {

    @Override
    public Response get(int id, String message) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        responseWrapper.setStatus(new StringBuilder(message).reverse().toString());
        return Response.ok()
                .entity(responseWrapper)
                .build();
    }

   @Override
    public Response get(CxfApiRequest cxfApiRequest) {
        return Response.ok()
                .entity(new StringBuilder(cxfApiRequest.getMessage()).reverse().toString())
                .build();
    }

}
