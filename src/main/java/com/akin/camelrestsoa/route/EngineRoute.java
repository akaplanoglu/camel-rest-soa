package com.akin.camelrestsoa.route;

import com.akin.camelrestsoa.aggregation.RequestAndResponseAggregationStrategy;
import com.akin.camelrestsoa.route.request.EngineDto;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class EngineRoute extends RouteBuilder {

    @Value("${server.port}")
    String serverPort;

    @Value("${camel.component.servlet.mapping.contextPath}")
    String contextPath;

    private static final String RESPONSE_STRING_FORMAT = "preference => %s\n";

    @Override
    public void configure() throws Exception {

        // This section is required - it tells Camel how to configure the REST service
        restConfiguration()
                .contextPath(contextPath)
                .apiContextPath("/api-doc")
                .apiProperty("api.title", "Camel Rest Soa Api")
                .apiProperty("api.version", "1.0")
                .apiProperty("cors", "true")
                .apiContextRouteId("doc-api")
                .port(serverPort)
                .host("localhost")
                // Use the 'servlet' component.
                // This tells Camel to create and use a Servlet to 'host' the RESTful API.
                // Since we're using Spring Boot, the default servlet container is Tomcat.
                .component("servlet")
                // Allow Camel to try to marshal/unmarshal between Java objects and JSON
                .bindingMode(RestBindingMode.auto)
                .dataFormatProperty("prettyPrint", "true");

        // if aggregated data should return as rest response. The request body can be stored in property.
        rest("/")
                .id("api-route")
                .get("/engine")
                .type(EngineDto.class) //Setting the request type enables Camel to unmarshal the request to a Java object
                .outType(EngineDto.class) //Setting the response type enables Camel to marshal the response to JSON
                .consumes(MediaType.APPLICATION_JSON_VALUE)
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route().routeId("rest-endpoint")
                .onException(Exception.class)
                .handled(true)
                .transform(simpleF(RESPONSE_STRING_FORMAT, exceptionMessage()))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(503))
                .end()
                .log("Incoming Body is ${body}")
                .to("{{route.remoteCxf}}")
                .end();

        from("{{route.remoteCxf}}")
                .routeId("call-cxf")
                .log("Outgoing pojo right before to CXF id: ${body.id} and CXF message: ${body.message} and Headers : ${in.headers}")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .enrich()
                .simple("http://localhost:8080/api/" + "${body.id}" + "/${body.message}" + "?bridgeEndpoint=true&throwExceptionOnFailure=false")
                .aggregationStrategy(new RequestAndResponseAggregationStrategy());


        // if same rest response should return immediately. Wiretap is used, we can also use seda component for asynchronous process.
        /*rest("/")
                .id("api-route")
                .get("/engine")
                .type(EngineDto.class) //Setting the request type enables Camel to unmarshal the request to a Java object
                .outType(EngineDto.class) //Setting the response type enables Camel to marshal the response to JSON
                .consumes(MediaType.APPLICATION_JSON_VALUE)
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route().routeId("rest-endpoint")
                .onException(Exception.class)
                .handled(true)
                .transform(simpleF(RESPONSE_STRING_FORMAT, exceptionMessage()))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(503))
                .end()
                .log("Incoming Body is ${body}")
                .wireTap("direct:tapCxf")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .transform(simple("${body}"))
                .end();

        from("direct:tapCxf")
                .routeId("call-cxf")
                .log("Outgoing pojo right before to CXF id: ${body.id} and CXF message: ${body.message} and Headers : ${in.headers}")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/xml"))
                .toD("http://localhost:8080/api/" + "${body.id}" + "/${body.message}" + "?bridgeEndpoint=true&throwExceptionOnFailure=false")
                .unmarshal().jacksonxml(true)
                .log("Cxf Response status: ${body}");*/

    }

    private JacksonDataFormat getJacksonDataFormat(Class<?> unmarshalType) {
        JacksonDataFormat format = new JacksonDataFormat();
        format.setUnmarshalType(unmarshalType);
        return format;
    }
}
