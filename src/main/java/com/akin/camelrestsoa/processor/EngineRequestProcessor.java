package com.akin.camelrestsoa.processor;

import com.akin.camelrestsoa.route.request.EngineDto;
import com.akin.camelrestsoa.rs.model.CxfApiRequest;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class EngineRequestProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {

        EngineDto engineDto = (EngineDto) exchange.getIn().getBody();
        CxfApiRequest cxfApiRequest = new CxfApiRequest();
        cxfApiRequest.setId(engineDto.getId());
        cxfApiRequest.setMessage(engineDto.getMessage());

        exchange.getIn().setBody(cxfApiRequest);
    }
}
